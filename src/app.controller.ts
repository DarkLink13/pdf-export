import { Controller, Get, Res } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private appService: AppService) {}

  @Get()
  async generateGraphAndPdf(@Res() res) {
    // Crear el gráfico utilizando Chart.js

    res.setHeader('Content-Type', 'application/pdf');
    res.setHeader('Content-Disposition', 'inline; filename=generated.pdf');

    const buffer = Buffer.from(this.appService.getPdf());

    // Devolver el stream como respuesta
    res.send(buffer);
  }
}
