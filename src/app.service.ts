import { Injectable } from '@nestjs/common';
import { createCanvas } from 'canvas';
import {
  Chart,
  LineElement,
  PointElement,
  RadarController,
  RadialLinearScale,
} from 'chart.js';
import { jsPDF } from 'jspdf';

@Injectable()
export class AppService {
  getPdf(): ArrayBuffer {
    const canvas = createCanvas(800, 400);
    const ctx = canvas.getContext('2d');
    Chart.register(
      RadarController,
      RadialLinearScale,
      PointElement,
      LineElement,
    );
    new Chart(ctx as any, {
      type: 'radar',
      data: {
        labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo'],
        datasets: [
          {
            label: 'Ventas mensuales',
            data: [12, 19, 3, 5, 2],
          },
        ],
      },
    });

    const doc = new jsPDF('p', 'pt', 'letter');
    doc.addImage(canvas.toBuffer(), 0, 0, 400, 800);
    return doc.output('arraybuffer');
  }
}
